import 'package:flutter/material.dart';
import 'package:network_call/model/datamodel.dart';

class DuoItem extends StatelessWidget {
  final Article first;
  final Article second;

  DuoItem({this.first, this.second});

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: getItemBody(first),
              ),
              Expanded(
                child: getItemBody(second),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center));
  }

  Widget getItemBody(Article article) {
    return Center(
        child: Card(
            elevation: 5,
            child: Column(
              children: <Widget>[
                Image.network(
                  article.picture.url,
                  filterQuality: FilterQuality.low,
                ),
                Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    article.title,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                )
              ],
            )));
  }
}
