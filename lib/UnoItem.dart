import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:network_call/model/datamodel.dart';

Widget getUnoCardWidget(Article article) {
  return Card(
    elevation: 5,
    child: UnoItem(article: article),
  );
}

class UnoItem extends StatelessWidget {
  final Article article;

  UnoItem({this.article});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 250,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image.network(
              article.picture.url,
              fit: BoxFit.fitWidth,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.black45,
              ),
              child: Text(
                article.title,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ));
  }
}
