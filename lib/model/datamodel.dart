const TRACKING_EVENTS = "trackingEvents";
const MAIN_CONTENT = "mainContent";

const DUO = "duo";
const UNO = "uno";
const CAROUSEL = "carousel";
const VIDEO_CAROUSEL = "videocarousel";

class Response {
  List<TrackingEvent> trackingEventsList;
  List<MainContent> mainContent;

  Response({this.trackingEventsList, this.mainContent});

  factory Response.fromJson(Map<String, dynamic> json) {
    var trackingEventsList = json[TRACKING_EVENTS] as List;
    var mainContent = json[MAIN_CONTENT] as List;

    return Response(
      trackingEventsList:
          trackingEventsList.map((i) => TrackingEvent.fromJson(i)).toList(),
      mainContent: mainContent.map((i) => MainContent.fromJson(i)).toList(),
    );
  }
}

class TrackingEvent {
  String event;

  TrackingEvent({this.event});

  factory TrackingEvent.fromJson(Map<String, dynamic> json) {
    return TrackingEvent(event: json['event'] as String);
  }
}

class MainContent {
  String type;
  List<Article> articleList;
  Article unoArticle;

  MainContent({this.type, this.articleList, this.unoArticle});

  factory MainContent.fromJson(Map<String, dynamic> json) {
    var trackingEventsList = json["articleList"] as List;
    var unoArticleJson = json['article'];

    return MainContent(
        unoArticle: (unoArticleJson == null) ? null : Article.fromJson(unoArticleJson),
        type: json['type'] as String,
        articleList:
            trackingEventsList?.map((i) => Article.fromJson(i))?.toList());
  }
}

class Item {
  String type;
  List<Article> articleList;

  Item({this.type, this.articleList});

  factory Item.fromJson(Map<String, dynamic> json) {
    var trackingEventsList = json["articleList"] as List;

    return Item(
        type: json['type'] as String,
        articleList:
            trackingEventsList?.map((i) => Article.fromJson(i))?.toList());
  }
}

class Article {
  String id;
  String title;
  String label;
  String introText;
  Picture picture;

  Article({this.id, this.title, this.label, this.introText, this.picture});

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
        id: json['id'] as String,
        title: json['title'] as String,
        label: json['label'] as String,
        introText: json['introText'] as String,
        picture: Picture.fromJson(json['picture']));
  }
}

class Picture {
  String url;
  int width;
  int height;

  Picture({this.url, this.height, this.width});

  factory Picture.fromJson(Map<String, dynamic> json) {
    return Picture(
        url: json['url'] as String,
        width: json['width'] as int,
        height: json['height'] as int);
  }
}
