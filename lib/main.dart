import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:network_call/DuoItem.dart';
import 'package:network_call/UnoItem.dart';
import 'package:network_call/VideoCarousel.dart';
import 'package:network_call/model/datamodel.dart';
import 'package:network_call/NewsCarosel.dart';

Future<List<MainContent>> fetchPhotos(http.Client client) async {
  final response =
      await client.get('https://mobileapi.ad.nl/mobile/sections/home?scale=4');

  return compute(parsePhotos, response.body);
}

// A function that will convert a response body into a List<Photo>
List<MainContent> parsePhotos(String responseBody) {
  final parsed = Response.fromJson(json.decode(responseBody));
  return parsed.mainContent;
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Flutter Demo';

    return MaterialApp(
      showPerformanceOverlay: false,
      title: appTitle,
      home: MyHomePage(title: appTitle),
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
//      bottomNavigationBar: KubusBottomNavigation(),
      body: FutureBuilder<List<MainContent>>(
        future: fetchPhotos(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? PhotosList(mainContentList: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class PhotosList extends StatelessWidget {
  final List<MainContent> mainContentList;

  PhotosList({Key key, this.mainContentList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: mainContentList.length,
      itemBuilder: (context, index) {
        final item = mainContentList[index];

        switch (item.type) {
          case DUO:
            return DuoItem(
              first: item.articleList.first,
              second: item.articleList.last,
            );
          case CAROUSEL:
            return NewsCarousel(articleList: item.articleList);
          case UNO:
            return getUnoCardWidget(item.unoArticle);
          case VIDEO_CAROUSEL:
            return VideoCarousel(articleList: item.articleList);
          default:
            return getAdBlock(item.type);
        }
      },
    );
  }

  Widget getAdBlock(String type) {
    return Center(
      child: Card(
        child: ListTile(
          title: Text(type),
          subtitle: const Text("In development"),
        ),
      ),
    );
  }
}
