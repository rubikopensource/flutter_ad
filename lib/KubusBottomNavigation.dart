
import 'package:flip_box_bar/flip_box_bar.dart';
import 'package:flutter/material.dart';

class KubusBottomNavigation extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    return FlipBoxBar(
      items: [
        FlipBarItem(
            icon: Icon(Icons.map),
            text: Text("Topic"),
            frontColor: Colors.blue,
            backColor: Colors.blueAccent),
        FlipBarItem(
            icon: Icon(Icons.add),
            text: Text("Add"),
            frontColor: Colors.cyan,
            backColor: Colors.cyanAccent),
        FlipBarItem(
            icon: Icon(Icons.chrome_reader_mode),
            text: Text("Read"),
            frontColor: Colors.orange,
            backColor: Colors.orangeAccent),
        FlipBarItem(
            icon: Icon(Icons.print),
            text: Text("Print"),
            frontColor: Colors.purple,
            backColor: Colors.purpleAccent),
        FlipBarItem(
            icon: Icon(Icons.event),
            text: Text("Event"),
            frontColor: Colors.pink,
            backColor: Colors.pinkAccent),
      ],
      onIndexChanged: (newIndex) {
        print(newIndex);
      },
    );  }

}