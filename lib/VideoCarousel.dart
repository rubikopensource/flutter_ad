import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:network_call/UnoItem.dart';
import 'package:network_call/model/datamodel.dart';

class VideoCarousel extends StatelessWidget {
  final List<Article> articleList;

  VideoCarousel({this.articleList});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      child: new Swiper(
        itemBuilder: (BuildContext context, int index) {
          return UnoItem(article: articleList[index]);
        },
        itemCount: articleList.length,
        pagination: new SwiperPagination(
          builder: SwiperPagination.rect,
        ),
        control: new SwiperControl(
          iconNext: null,
          iconPrevious: null,
        ),
        viewportFraction: 0.8,
        scale: 0.9,
      ),
    );
  }
}
