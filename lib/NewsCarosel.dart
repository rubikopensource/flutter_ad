import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:network_call/UnoItem.dart';
import 'package:network_call/model/datamodel.dart';

class NewsCarousel extends StatelessWidget {
  final List<Article> articleList;

  NewsCarousel({this.articleList});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      child: new Swiper(
        itemBuilder: (BuildContext context, int index) {
          return UnoItem(article: articleList[index]);
        },
        itemCount: articleList.length,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
      ),
    );
  }
}
